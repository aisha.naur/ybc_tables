import os, sys

if (len(sys.argv) != 3):
    print('upload.py usage: python upload.py FILTER YBC/rYBC/iYBC')
    sys.exit(1)

FILTER=sys.argv[1]
YBC=sys.argv[2]
cmd='git lfs pull --include "'+YBC+'/'+FILTER+'/*/*"'
print(cmd)
os.system(cmd)
exit(0)
